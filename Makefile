prefix := /usr/local

bindir := $(prefix)/bin
sharedir := $(prefix)/share
docdir := $(sharedir)/doc
mandir := $(sharedir)/man

ifeq ($(shell pkg-config --atleast-version 1.9 mozilla-gtkmozembed-embedding && echo yes),yes)
    moz_name := xulrunner-1.9
    moz_pc := mozilla-gtkmozembed-embedding
    moz_cppflags_extra := $(shell pkg-config --cflags xulrunner-nspr) -DXPCOM_GLUE_USE_NSPR
    moz_unstable_cppflags_extra :=
    moz_ldflags_extra := $(shell pkg-config --libs xulrunner-nspr)
    # libxpcomglue needs libdl, but mozilla-gtkmozembed-embedding.pc
    # doesn't mention it.
    moz_ldflags_extra += -ldl
else
    ifeq ($(shell pkg-config --exists xulrunner-gtkmozembed && echo yes),yes)
    moz_name := xulrunner
    moz_pc := xulrunner-gtkmozembed
    else
    moz_name := mozilla
    moz_pc := mozilla-gtkmozembed
    endif
    moz_unstable_cppflags_extra = \
	$(addprefix -I$(moz_include_dir)/, \
          content docshell dom gfx imglib2 layout locale necko uconv webshell widget) \
	-DMOZILLA_INTERNAL_API
    moz_ldflags_extra = -Wl,-rpath,$(moz_lib_dir)
endif

moz_prefix := $(shell pkg-config --variable=prefix $(moz_pc))
moz_include_dir := $(shell pkg-config --variable=includedir $(moz_pc))
moz_lib_dir := $(moz_prefix)/lib/$(moz_name)

moz_version := $(shell pkg-config --modversion $(moz_pc))
# HACK: Replace 'b' for beta with micro=-1
moz_version := $(subst b,.-1.,$(moz_version))
moz_version_major := $(word 1,$(subst ., ,$(moz_version)))
moz_version_minor := $(word 2,$(subst ., ,$(moz_version)))
moz_version_micro := $(word 3,$(subst ., ,$(moz_version)))

CFLAGS := -ansi -Wall -Wunused -Wno-unused-parameter
CPPFLAGS := -D_REENTRANT
CXXFLAGS := -ansi -Wall -Wunused
LDFLAGS := -lpthread                                      	\
           $(shell pkg-config --libs gtkmm-2.4 $(moz_pc))	\
           $(moz_ldflags_extra) -lexpat -lX11

ifdef NDEBUG
    CPPFLAGS += -DNDEBUG
else
    CFLAGS += -g
    CXXFLAGS += -g
    LDFLAGS += -g
endif

cxxsources := \
    auto_proc.cpp browser_widget.cpp child_iterator.cpp                    \
    event_state_manager.cpp generate_dvd.cpp link_iterator.cpp             \
    null_prompt_service.cpp pixbufs.cpp style_sheets.cpp temp_file.cpp     \
    video.cpp vob_list.cpp videolink.cpp warp_pointer.cpp                  \
    x_frame_buffer.cpp xml_utils.cpp xpcom_support.cpp
csources := jquant2.c

sources_using_gtkmm :=                                                     \
    browser_widget.cpp generate_dvd.cpp pixbufs.cpp temp_file.cpp          \
    vob_list.cpp videolink.cpp warp_pointer.cpp
sources_using_moz :=                                                       \
    browser_widget.cpp child_iterator.cpp event_state_manager.cpp          \
    link_iterator.cpp null_prompt_service.cpp style_sheets.cpp             \
    videolink.cpp xpcom_support.cpp
sources_using_moz_unstable := \
    browser_widget.cpp event_state_manager.cpp link_iterator.cpp           \
    null_prompt_service.cpp style_sheets.cpp videolink.cpp

videolink : $(cxxsources:%.cpp=.objs/%.o) $(csources:%.c=.objs/%.o)
	$(CXX) $^ $(LDFLAGS) -o $@

clean :
	rm -rf .objs
	rm -f videolink *~ .\#* *.orig *.rej svn-commit*.tmp

install :
	mkdir -p -m 755 $(DESTDIR)$(bindir)
	install -m 755 $(if $(NDEBUG),-s,) videolink $(DESTDIR)$(bindir)
	mkdir -p -m 755 $(DESTDIR)$(docdir)/videolink
	gzip -c9 README >$(DESTDIR)$(docdir)/videolink/README.gz
	gzip -c9 ChangeLog >$(DESTDIR)$(docdir)/videolink/ChangeLog.gz
	chmod 644 $(DESTDIR)$(docdir)/videolink/*.gz
	mkdir -p -m 755 $(DESTDIR)$(mandir)/man1
	gzip -c9 videolink.1 >$(DESTDIR)$(mandir)/man1/videolink.1.gz
	chmod 644 $(DESTDIR)$(mandir)/man1/videolink.1.gz
	mkdir -p -m 755 $(DESTDIR)$(sharedir)/videolink
	install -m 644 *.css $(DESTDIR)$(sharedir)/videolink

.PHONY : clean install

.objs/browser_widget.% : CPPFLAGS += -DMOZ_LIB_DIR='"$(moz_lib_dir)"'

.objs/videolink.% \
    : CPPFLAGS += -DVIDEOLINK_SHARE_DIR='"$(sharedir)/videolink"'

$(sources_using_gtkmm:%.cpp=.objs/%.o) \
    : CPPFLAGS += $(shell pkg-config --cflags gtkmm-2.4)

$(sources_using_moz:%.cpp=.objs/%.o) \
    : CPPFLAGS += $(filter-out -fshort-wchar, \
		    $(shell pkg-config --cflags $(moz_pc)) $(moz_cppflags_extra))
# Non-virtual destructors are fine in XPCOM interface classes since
# instances are only ever called by the Release function which is virtual.
$(sources_using_moz:%.cpp=.objs/%.o) : CXXFLAGS += -Wno-non-virtual-dtor

$(sources_using_moz_unstable:%.cpp=.objs/%.o)                          \
    : CPPFLAGS += $(moz_unstable_cppflags_extra)                       \
                  -DMOZ_VERSION_MAJOR=$(moz_version_major)             \
                  -DMOZ_VERSION_MINOR=$(moz_version_minor)             \
                  -DMOZ_VERSION_MICRO=$(moz_version_micro)

.objs/%.o : %.cpp .objs/.created
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $@ -MD -MF .objs/$*.d -c $<

.objs/%.o : %.c .objs/.created
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -MD -MF .objs/$*.d -c $<

%/.created :
	mkdir -p $*
	touch $@

ifneq ($(MAKECMDGOALS),clean)
    -include $(cxxsources:%.cpp=.objs/%.d) $(csources:%.c=.objs/%.d)
endif

.PRECIOUS : %/.created
