#ifndef INC_DVD_HPP
#define INC_DVD_HPP

namespace dvd
{
    // Maximum number of buttons in a menu.
    const unsigned menu_buttons_max = 36;

    // Number of colours allowed in each button.
    // Buttons can change colour when they are selected.
    const unsigned button_n_colours = 4;

    // DVD virtual machine register size.
    const unsigned reg_bits = 16;

    // Number by which button numbers must be multiplied when stored in
    // system register 8.
    const unsigned reg_s8_button_mult = 0x400;

    // Maximum number of PGCs in a domain that can be linked to.
    const unsigned domain_pgcs_max = 127;

    // Maximum number of titles on a disc.  (Also the maximum number of
    // titlesets per disc.)
    const unsigned titles_max = 99;
}

#endif // !INC_DVD_HPP
