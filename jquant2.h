#ifndef INC_JQUANT2_H
#define INC_JQUANT2_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	JDITHER_NONE,		/* no dithering */
	JDITHER_FS		/* Floyd-Steinberg error diffusion dither */
} J_DITHER_MODE;

typedef unsigned char JSAMPLE;
typedef JSAMPLE * JSAMPROW;
typedef JSAMPROW * JSAMPARRAY;

void quantize (JSAMPARRAY input_buf,
	       JSAMPARRAY output_buf,
	       int width, int height,
	       J_DITHER_MODE dither_mode,
	       int desired_number_of_colors,
	       unsigned int * output_colors);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* !INC_JQUANT2_H */
