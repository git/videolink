// Copyright 2005-6 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#include "style_sheets.hpp"

#include "wchar_t_short.h"
#include <nsCOMPtr.h>
#include <nsContentCID.h>
#include <nsIStyleSheetService.h>
#include <nsServiceManagerUtils.h>
#include <nsIURI.h>
#include <nsNetUtil.h>
#include "wchar_t_default.h"

#include "xpcom_support.hpp"

using xpcom_support::check;

// We just have to load and register a style-sheet as a user
// style-sheet.  There is no need to do anything for each page.

void init_agent_style_sheet(const char * uri)
{
    nsCOMPtr<nsIURI> style_sheet_uri;
    check(NS_NewURI(getter_AddRefs(style_sheet_uri), nsCString(uri)));

    nsCOMPtr<nsIStyleSheetService> style_sheet_service;
    static const nsCID style_sheet_service_cid = {
	// NS_STYLESHEETSERVICE_CID copied from
	// layout/base/nsStyleSheetService.cpp
	0xfcca6f83, 0x9f7d, 0x44e4,
	{0xa7, 0x4b, 0xb5, 0x94, 0x33, 0xe6, 0xc8, 0xc3}
    };
    check(CallGetService<nsIStyleSheetService>(
	      style_sheet_service_cid, getter_AddRefs(style_sheet_service)));
    check(style_sheet_service->LoadAndRegisterSheet(
	      style_sheet_uri, nsIStyleSheetService::USER_SHEET));
}
