// Copyright 2006 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_VOB_LIST_HPP
#define INC_VOB_LIST_HPP

#include <list>
#include <stdexcept>
#include <string>

struct vob_ref
{
    std::string file;		// file name (absolute, resolved rel. to list)
    std::string chapters;	// chapters attribute, unmodified
    std::string pause;		// pause attribute, unmodified
};

typedef std::list<vob_ref> vob_list;

vob_list read_vob_list(const std::string & file_name);

struct xml_error : public std::runtime_error
{
    xml_error(const std::string & path, int line, const std::string & message);
};

#endif // !INC_VOB_LIST_HPP
