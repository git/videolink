#ifndef INC_NULL_PROMPT_SERVICE_HPP
#define INC_NULL_PROMPT_SERVICE_HPP

#include "wchar_t_short.h"
#include <nsIPromptService.h>
#include "wchar_t_default.h"

class null_prompt_service : public nsIPromptService
{
public:
    static void install();

    NS_DECL_ISUPPORTS
    NS_DECL_NSIPROMPTSERVICE
};

#endif // !INC_NULL_PROMPT_SERVICE_HPP
