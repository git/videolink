// Copyright 2006 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#include <gdkmm/window.h>

// Move the pointer by (dx, dy) relative to its current position on
// the display which window appears on.  
// This is separated from videolink.cpp solely because it uses Xlib
// and Xlib.h defines many macros we don't want.
void warp_pointer(const Glib::RefPtr<Gdk::Window> & window, int dx, int dy);
