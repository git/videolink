// Copyright 2005-6 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_PIXBUFS_HPP
#define INC_PIXBUFS_HPP

#include <glibmm/refptr.h>

namespace Gdk
{
    class Pixbuf;
}

// Find pixel differences between an "old" and "new" RGB Pixbuf
// (or RGBA, but the alpha component will be ignored) and copy the
// differing pixels from the new one to a third RGBA Pixbuf at the
// specified offset with full opacity.
// The width and height of the new Pixbufs must be equal and match
// the specified dimensions.  The width and height of the old and
// third Pixbuf must be large enough to store a rectangle of
// those dimensions at the specified offset.
void diff_rgb_pixbufs(Glib::RefPtr<Gdk::Pixbuf> old_buf,
		      Glib::RefPtr<Gdk::Pixbuf> new_buf,
		      Glib::RefPtr<Gdk::Pixbuf> diff_buf,
		      int offset_x, int offset_y,
		      int width, int height);

// Quantise an RGBA Pixbuf to the specified number of colours, including
// one transparent colour.  Currently uses Floyd-Steinberg dithering.
void quantise_rgba_pixbuf(Glib::RefPtr<Gdk::Pixbuf> buf, int n_colours);

#endif // !INC_PIXBUFS_HPP
