// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#include <cassert>

#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <wait.h>

#include "auto_proc.hpp"

void auto_kill_proc_closer::operator()(pid_t pid) const
{
    assert(pid >= -1);

    if (pid > 0 && waitpid(pid, NULL, WNOHANG) == 0)
    {
	kill(pid, SIGTERM);
	while (waitpid(pid, NULL, 0) == -1)
	    if (errno != EINTR)
	    {
		assert(!"invalid pid in auto_kill_proc_closer");
		break;
	    }
    }
}

void auto_wait_proc_closer::operator()(pid_t pid) const
{
    assert(pid >= -1);

    if (pid > 0)
	while (waitpid(pid, NULL, 0) == -1)
	    if (errno != EINTR)
	    {
		assert(!"invalid pid in auto_wait_proc_closer");
		break;
	    }
}
