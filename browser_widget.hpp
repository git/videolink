// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_BROWSER_WIDGET_HPP
#define INC_BROWSER_WIDGET_HPP

#include <glibmm/signalproxy.h>
#include <gtkmm/bin.h>

#include "wchar_t_short.h"
#include <gtkmozembed.h>
#include <nsCOMPtr.h>
#include "wchar_t_default.h"

class browser_widget;
class nsIWebBrowser;

namespace Glib
{
    browser_widget * wrap(GtkMozEmbed * object, bool take_copy = false);
}

class browser_widget : public Gtk::Bin
{
public:
    browser_widget();
    virtual ~browser_widget();
    GtkMozEmbed * gobj();
    const GtkMozEmbed * gobj() const;

    void load_uri(const char * uri);
    void load_uri(const std::string & uri);
    void stop_load();
    void go_back();
    void go_forward();
    void reload(gint32 flags = GTK_MOZ_EMBED_FLAG_RELOADNORMAL);

    bool can_go_back() const;
    bool can_go_forward() const;

    std::string get_link_message() const;
    std::string get_js_status() const;
    std::string get_title() const;
    std::string get_location() const;
    already_AddRefed<nsIWebBrowser> get_browser();

    Glib::SignalProxy0<void> signal_link_message();
    Glib::SignalProxy0<void> signal_js_status();
    Glib::SignalProxy0<void> signal_location();
    Glib::SignalProxy0<void> signal_title();
    Glib::SignalProxy2<void, gint /*cur*/, gint /*max*/> signal_progress();
    Glib::SignalProxy3<void, const char *, gint /*flags*/, guint /*status*/>
	signal_net_state();
    Glib::SignalProxy0<void> signal_net_start();
    Glib::SignalProxy0<void> signal_net_stop();
    Glib::SignalProxy1<browser_widget *, guint /*chromemask*/> signal_new_window();
    Glib::SignalProxy1<void, bool /*visibility*/> signal_visibility();
    Glib::SignalProxy0<void> signal_destroy();
    Glib::SignalProxy1<bool, const char * /*uri*/> signal_open_uri();

    // This must be instantiated after Gtk initialisation and before
    // instantiation of browser_widget.
    struct initialiser
    {
	initialiser();
	~initialiser();
    };

private:
    browser_widget(GObject * gobject, bool take_copy);
    static Glib::ObjectBase * wrap_new(GObject * gobject);
    friend browser_widget * Glib::wrap(GtkMozEmbed * object, bool take_copy);
};

#endif // !INC_BROWSER_WIDGET_HPP
