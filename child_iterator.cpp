// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#include "child_iterator.hpp"

#include <cassert>

#include "xpcom_support.hpp"

using xpcom_support::check;

child_iterator::child_iterator()
	: node_(0)
{}

child_iterator::child_iterator(nsIDOMNode * node)
{
    check(node->GetFirstChild(&node_));
}

child_iterator::~child_iterator()
{
    if (node_)
	node_->Release();
}

already_AddRefed<nsIDOMNode> child_iterator::operator*() const
{
    assert(node_);
    node_->AddRef();
    return node_;
}

child_iterator & child_iterator::operator++()
{
    nsIDOMNode * next;
    check(node_->GetNextSibling(&next));
    node_->Release();
    node_ = next;
    return *this;
}

bool child_iterator::operator==(const child_iterator & other) const
{
    return node_ == other.node_;
}
