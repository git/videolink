// Copyright 2005-6 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_STYLESHEETS_HPP
#define INC_STYLESHEETS_HPP

// Load agent style sheet from an (absolute) URI and register it.
void init_agent_style_sheet(const char * uri);

#endif // !INC_STYLESHEETS_HPP
