// Copyright 2006 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_XML_UTILS_HPP
#define INC_XML_UTILS_HPP

#include <string>

std::string xml_escape(const std::string & str);

#endif // !INC_XML_UTILS_HPP
