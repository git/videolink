// Copyright 2005-6 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_GENERATE_DVD_HPP
#define INC_GENERATE_DVD_HPP

#include <string>
#include <vector>

#include <glibmm/refptr.h>

#include "geometry.hpp"
#include "temp_file.hpp"
#include "video.hpp"
#include "vob_list.hpp"

namespace Gdk
{
    class Pixbuf;
}

// Description of menus and titles to go on a DVD.

class dvd_generator
{
public:
    enum pgc_type { unknown_pgc,  menu_pgc, title_pgc };

    // Reference to some PGC (program chain).
    struct pgc_ref
    {
	explicit pgc_ref(pgc_type type = unknown_pgc,
			 int index = -1,
			 int sub_index = 0)
		: type(type), index(index), sub_index(sub_index)
	    {}
	bool operator==(const pgc_ref & other) const
	    {
		return type == other.type && index == other.index;
	    }
	bool operator!=(const pgc_ref & other) const
	    {
		return !(*this == other);
	    }
	    
	pgc_type type;      // Menu or title reference?
	unsigned index;     // Menu or title index (within resp. vector)
	unsigned sub_index; // Button or chapter number (1-based; 0 if
	                    // unspecified; not compared!)
    };

    // We can try using either of these encoders to convert PNG to MPEG.
    enum mpeg_encoder
    {
	mpeg_encoder_ffmpeg,         // ffmpeg
	mpeg_encoder_mjpegtools      // mjpegtools from version 1.8
    };

    dvd_generator(const video::frame_params & frame_params,
		  mpeg_encoder encoder);

    // Create a new empty menu; return a reference to it.
    // The client must call generate_menu_vob() for each menu before
    // calling generate().
    pgc_ref add_menu();
    // Add a menu entry (link) to an existing menu.
    void add_menu_entry(unsigned index,
			const rectangle & area,
			const pgc_ref & target);
    // Generate the menu VOB from a background image and button
    // highlight image.
    void generate_menu_vob(unsigned index,
			   Glib::RefPtr<Gdk::Pixbuf> background,
			   Glib::RefPtr<Gdk::Pixbuf> highlights) const;

    // Create a new title using the given vob_list; return a reference
    // to it.  The argument will be pilfered (i.e. emptied).
    pgc_ref add_title(vob_list & list);

    // Use dvdauthor to generate a DVD filesystem.
    void generate(const std::string & output_dir) const;

private:
    struct menu_entry
    {
	rectangle area;
	pgc_ref target;
    };

    // Menu definition.
    struct menu
    {
	// References to the menus and titles that the menu buttons
	// are meant to link to, in the same order as the buttons.
	std::vector<menu_entry> entries;
    };

    temp_dir temp_dir_;
    video::frame_params frame_params_;
    mpeg_encoder encoder_;
    std::vector<menu> menus_;
    std::vector<vob_list> titles_;
};

#endif // !INC_GENERATE_DVD_HPP
