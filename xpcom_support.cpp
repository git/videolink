// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#include <cstdio>
#include <cassert>
#include <memory>
#include <stdexcept>

#include "xpcom_support.hpp"

namespace xpcom_support
{
    void throw_exception(nsresult error)
    {
	assert(NS_ERROR_GET_SEVERITY(error) == NS_ERROR_SEVERITY_ERROR);

	// TODO: look up error message
	char message[30];
	std::sprintf(message, "XPCOM error %08x", error);

	switch (error)
	{
	case NS_ERROR_OUT_OF_MEMORY:
	    throw std::bad_alloc();

	case NS_ERROR_NOT_INITIALIZED:
	case NS_ERROR_ALREADY_INITIALIZED:
	case NS_ERROR_INVALID_POINTER:
	case NS_ERROR_ILLEGAL_VALUE:
	case NS_BASE_STREAM_CLOSED:
	case NS_BASE_STREAM_ILLEGAL_ARGS:
	    assert(!"internal error detected by XPCOM function");
	    throw std::logic_error(message);

	default:
	    throw std::runtime_error(message);
	}
    }
}
