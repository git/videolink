// Copyright 2005-8 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#include <cassert>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <queue>
#include <set>
#include <sstream>
#include <string>

#include <stdlib.h>

#include <gdk/gdkkeysyms.h>
#include <gdkmm/pixbuf.h>
#include <glibmm/convert.h>
#include <glibmm/spawn.h>
#include <gtkmm/main.h>
#include <gtkmm/window.h>

#include "videolink.hpp"
#include "wchar_t_short.h"
#include <ImageErrors.h>
#if MOZ_VERSION_GE(1,9,0)
#include <nsWeakPtr.h>
/* For some reason <nsWeakPtr.h> no longer defines this */
typedef nsCOMPtr<nsIWeakReference> nsWeakPtr;
#endif
#include <nsGUIEvent.h>
#include <nsIBoxObject.h>
#include <nsIContent.h>
#include <nsIDocShell.h>
#include <nsIDOMAbstractView.h>
#include <nsIDOMBarProp.h>
#include <nsIDOMDocumentEvent.h>
#include <nsIDOMDocumentView.h>
#include <nsIDOMElement.h>
#include <nsIDOMEventTarget.h>
#include <nsIDOMHTMLDocument.h>
#include <nsIDOMMouseEvent.h>
#include <nsIDOMNSDocument.h>
#include <nsIDOMWindow.h>
#include <nsIEventStateManager.h>
#include <nsIInterfaceRequestorUtils.h>
#include <nsIURI.h> // required before nsILink.h
#include <nsILink.h>
#include <nsIPrefBranch.h>
#include <nsIPrefService.h>
#include <nsIPresShell.h>
#include <nsServiceManagerUtils.h>
#include <nsIWebBrowser.h>
#ifdef MOZILLA_INTERNAL_API
#include <nsString.h>
#else
#include <nsStringAPI.h>
#endif
#include "wchar_t_default.h"

#include "browser_widget.hpp"
#include "child_iterator.hpp"
#include "dvd.hpp"
#include "event_state_manager.hpp"
#include "generate_dvd.hpp"
#include "geometry.hpp"
#include "link_iterator.hpp"
#include "null_prompt_service.hpp"
#include "pixbufs.hpp"
#include "style_sheets.hpp"
#include "temp_file.hpp"
#include "video.hpp"
#include "warp_pointer.hpp"
#include "x_frame_buffer.hpp"
#include "xml_utils.hpp"
#include "xpcom_support.hpp"

using xpcom_support::check;

namespace
{
#if MOZ_VERSION_GE(2,0,-1)
    rectangle get_elem_rect(nsIDocument * doc, nsIDOMElement * elem)
#else
    rectangle get_elem_rect(nsIDOMNSDocument * doc, nsIDOMElement * elem)
#endif
    {
	rectangle result;

	// Start with this element's bounding box
	nsCOMPtr<nsIBoxObject> box;
	check(doc->GetBoxObjectFor(elem, getter_AddRefs(box)));
	int width, height;
	check(box->GetScreenX(&result.left));
	check(box->GetScreenY(&result.top));
	check(box->GetWidth(&width));
	check(box->GetHeight(&height));
	result.right = result.left + width;
	result.bottom = result.top + height;

	// Merge bounding boxes of all child elements
	for (child_iterator it = child_iterator(elem), end; it != end; ++it)
	{
	    nsCOMPtr<nsIDOMNode> child_node(*it);
	    PRUint16 child_type;
	    if (check(child_node->GetNodeType(&child_type)),
		child_type == nsIDOMNode::ELEMENT_NODE)
	    {
		nsCOMPtr<nsIDOMElement> child_elem(
		    do_QueryInterface(child_node));
		result |= get_elem_rect(doc, child_elem);
	    }
	}

	return result;
    }


    enum video_format
    {
	video_format_none,
	video_format_mpeg2_ps,
	video_format_vob_list
    };

    video_format video_format_from_uri(const std::string & uri)
    {
	// FIXME: This is a bit of a hack.  Perhaps we could decide
	// later based on the MIME type determined by Mozilla?
	static struct {
	    const char * extension;
	    video_format format;
	} const mapping[] = {
	    {".vob",     video_format_mpeg2_ps},
	    {".mpeg",    video_format_mpeg2_ps},
	    {".mpeg2",   video_format_mpeg2_ps},
	    {".mpg",     video_format_mpeg2_ps},
	    {".voblist", video_format_vob_list}
	};
	for (std::size_t i = 0;
	     i != sizeof(mapping) / sizeof(mapping[0]);
	     ++i)
	{
	    std::size_t ext_len = std::strlen(mapping[i].extension);
	    if (uri.size() > ext_len
		&& uri.compare(uri.size() - ext_len, ext_len,
			       mapping[i].extension) == 0)
		return mapping[i].format;
	}
	return video_format_none;
    }


    class base_window : public Gtk::Window
    {
    public:
	base_window(const video::frame_params & frame_params);

    protected:
	video::frame_params frame_params_;
	browser_widget browser_widget_;
    };

    base_window::base_window(const video::frame_params & frame_params)
	: frame_params_(frame_params)
    {
	set_size_request(frame_params_.width, frame_params_.height);
	set_resizable(false);

	add(browser_widget_);
	browser_widget_.show();
    }

    class preview_window : public base_window
    {
    public:
	preview_window(const video::frame_params & frame_params,
		       const std::string & main_page_uri);

    private:
	bool on_idle();
	bool on_key_press(GdkEventKey *);

	std::string main_page_uri_;
    };

    preview_window::preview_window(const video::frame_params & frame_params,
				   const std::string & main_page_uri)
	: base_window(frame_params),
	  main_page_uri_(main_page_uri)
    {
	Glib::signal_idle().connect(
	    sigc::mem_fun(this, &preview_window::on_idle));
	signal_key_press_event().connect(
	    sigc::mem_fun(this, &preview_window::on_key_press));
    }

    bool preview_window::on_idle()
    {
	browser_widget_.load_uri(main_page_uri_);
	return false; // don't call again
    }

    bool preview_window::on_key_press(GdkEventKey * event)
    {
	switch (event->keyval)
	{
	case GDK_t: // = top menu
	    browser_widget_.load_uri(main_page_uri_);
	    return true;
	case GDK_q: // = quit
	    Gtk::Main::quit();
	    return true;
	default:
	    return false;
	}
    }

    class conversion_window : public base_window
    {
    public:
	conversion_window(const video::frame_params & frame_params,
			  const std::string & main_page_uri,
			  const std::string & output_dir,
			  dvd_generator::mpeg_encoder encoder);

	bool is_finished() const;

    private:
	struct page_state;

	dvd_generator::pgc_ref add_menu(const std::string & uri);
	dvd_generator::pgc_ref add_title(const std::string & uri,
					 video_format format);
	void load_next_page();
	bool on_idle();
	void on_net_state_change(const char * uri, gint flags, guint status);
	bool browser_is_busy() const
	    {
		return pending_window_update_ || pending_req_count_;
	    }
	// Do as much processing as possible.  Return a flag indicating
	// whether to call again once the browser is idle.
	bool process();
	// Return a Pixbuf containing a copy of the window contents.
	Glib::RefPtr<Gdk::Pixbuf> get_screenshot();
	// Do as much processing as possible on the page links.  Return
	// a flag indicating whether to call again once the browser is
	// idle.
	bool process_links(
	    page_state * state,
	    nsIDOMDocument * basic_doc,
	    nsIDocShell * doc_shell,
	    nsIDOMWindow * dom_window);

	std::string output_dir_;

	enum {
	    state_initial,
	    state_processing,
	    state_finished
	} state_;

	dvd_generator generator_;
	typedef std::map<std::string, dvd_generator::pgc_ref>
	    resource_map_type;
	resource_map_type resource_map_;

	std::queue<std::string> page_queue_;
	bool pending_window_update_;
	int pending_req_count_;
	std::auto_ptr<page_state> page_state_;
    };

    conversion_window::conversion_window(
	const video::frame_params & frame_params,
	const std::string & main_page_uri,
	const std::string & output_dir,
	dvd_generator::mpeg_encoder encoder)
	: base_window(frame_params),
	  output_dir_(output_dir),
	  state_(state_initial),
	  generator_(frame_params, encoder),
	  pending_window_update_(false),
	  pending_req_count_(0)
    {
	Glib::signal_idle().connect(
	    sigc::mem_fun(this, &conversion_window::on_idle));
	browser_widget_.signal_net_state().connect(
	    sigc::mem_fun(this, &conversion_window::on_net_state_change));

	add_menu(main_page_uri);
    }

    bool conversion_window::is_finished() const
    {
	return state_ == state_finished;
    }

    dvd_generator::pgc_ref conversion_window::add_menu(const std::string & uri)
    {
	dvd_generator::pgc_ref & pgc_ref = resource_map_[uri];
	if (pgc_ref.type == dvd_generator::unknown_pgc)
	{
	    pgc_ref = generator_.add_menu();
	    page_queue_.push(uri);
	}
	return pgc_ref;
    }

    dvd_generator::pgc_ref conversion_window::add_title(const std::string & uri,
						      video_format format)
    {
	dvd_generator::pgc_ref & pgc_ref = resource_map_[uri];

	if (pgc_ref.type == dvd_generator::unknown_pgc)
	{
	    Glib::ustring hostname;
	    std::string path(Glib::filename_from_uri(uri, hostname));
	    // FIXME: Should check the hostname

	    vob_list list;

	    // Store a reference to a linked VOB file, or the contents
	    // of a linked VOB list file.
	    if (format == video_format_mpeg2_ps)
	    {
		if (!Glib::file_test(path, Glib::FILE_TEST_IS_REGULAR))
		    throw std::runtime_error(
			path + " is missing or not a regular file");
		vob_ref ref;
		ref.file = path;
		list.push_back(ref);
	    }
	    else if (format == video_format_vob_list)
	    {
		read_vob_list(path).swap(list);
	    }
	    else
	    {
		assert(!"unrecognised format in add_title");
	    }

	    pgc_ref = generator_.add_title(list);
	}

	return pgc_ref;
    }

    void conversion_window::load_next_page()
    {
	assert(!page_queue_.empty());
	const std::string & uri = page_queue_.front();
	std::cout << "INFO: Loading <" << uri << ">" << std::endl;

	browser_widget_.load_uri(uri);
    }

    void conversion_window::on_net_state_change(const char * uri,
						gint flags, guint status)
    {
#       ifdef DEBUG_ON_NET_STATE_CHANGE
	std::cout << "conversion_window::on_net_state_change(";
	if (uri)
	    std::cout << '"' << uri << '"';
	else
	    std::cout << "NULL";
	std::cout << ", ";
	{
	    gint flags_left = flags;
	    static const struct {
		gint value;
		const char * name;
	    } flag_names[] = {
		{ GTK_MOZ_EMBED_FLAG_START, "STATE_START" },
		{ GTK_MOZ_EMBED_FLAG_REDIRECTING, "STATE_REDIRECTING" },
		{ GTK_MOZ_EMBED_FLAG_TRANSFERRING, "STATE_TRANSFERRING" },
		{ GTK_MOZ_EMBED_FLAG_NEGOTIATING, "STATE_NEGOTIATING" },
		{ GTK_MOZ_EMBED_FLAG_STOP, "STATE_STOP" },
		{ GTK_MOZ_EMBED_FLAG_IS_REQUEST, "STATE_IS_REQUEST" },
		{ GTK_MOZ_EMBED_FLAG_IS_DOCUMENT, "STATE_IS_DOCUMENT" },
		{ GTK_MOZ_EMBED_FLAG_IS_NETWORK, "STATE_IS_NETWORK" },
		{ GTK_MOZ_EMBED_FLAG_IS_WINDOW, "STATE_IS_WINDOW" }
	    };
	    for (int i = 0; i != sizeof(flag_names)/sizeof(flag_names[0]); ++i)
	    {
		if (flags & flag_names[i].value)
		{
		    std::cout << flag_names[i].name;
		    flags_left -= flag_names[i].value;
		    if (flags_left)
			std::cout << " | ";
		}
	    }
	    if (flags_left)
		std::cout << "0x" << std::setbase(16) << flags_left;
	}
	std::cout << ", " << "0x" << std::setbase(16) << status << ")\n";
#       endif // DEBUG_ON_NET_STATE_CHANGE

	if (flags & GTK_MOZ_EMBED_FLAG_IS_REQUEST)
	{
	    if (flags & GTK_MOZ_EMBED_FLAG_START)
		++pending_req_count_;

	    if (flags & GTK_MOZ_EMBED_FLAG_STOP)
	    {
		assert(pending_req_count_ != 0);
		--pending_req_count_;
	    }
	}
	    
	if (flags & GTK_MOZ_EMBED_FLAG_IS_DOCUMENT
	    && flags & GTK_MOZ_EMBED_FLAG_START)
	{
	    pending_window_update_ = true;
	}

	if (flags & GTK_MOZ_EMBED_FLAG_IS_WINDOW
	    && flags & GTK_MOZ_EMBED_FLAG_STOP)
	{
	    // Check whether the load was successful, ignoring this
	    // pseudo-error.
#ifdef NS_IMAGELIB_ERROR_LOAD_ABORTED
	    if (status != NS_IMAGELIB_ERROR_LOAD_ABORTED)
#endif
		check(status);

	    pending_window_update_ = false;
	}
    }

    struct conversion_window::page_state
    {
	page_state(Glib::RefPtr<Gdk::Pixbuf> norm_pixbuf,
		   nsIDOMDocument * doc, int width, int height)
		: norm_pixbuf(norm_pixbuf),
		  diff_pixbuf(Gdk::Pixbuf::create(
				  Gdk::COLORSPACE_RGB,
				  true, 8, // has_alpha, bits_per_sample
				  width, height)),
		  links_it(doc),
		  link_changing(false)
	    {
	    }

	Glib::RefPtr<Gdk::Pixbuf> norm_pixbuf;
	Glib::RefPtr<Gdk::Pixbuf> diff_pixbuf;

	link_iterator links_it, links_end;

	rectangle link_rect;
	std::string link_target;
	bool link_changing;
    };

    bool conversion_window::on_idle()
    {
	if (state_ == state_initial)
	{
	    // Put pointer in the top-left so that no links appear in
	    // the hover state when we take a screenshot.
	    warp_pointer(get_window(),
			 -frame_params_.width, -frame_params_.height);

	    load_next_page();

	    state_ = state_processing;
	}
	else if (state_ == state_processing && !browser_is_busy())
	{
	    try
	    {
		if (!process())
		{
		    state_ = state_finished;
		    Gtk::Main::quit();
		}
	    }
	    catch (...)
	    {
		// Print context of exception.
		if (!page_queue_.empty())
		{
		    std::cerr << "ERROR: While processing page <"
			      << page_queue_.front() << ">:\n";
		    if (page_state_.get() && !page_state_->link_target.empty())
			std::cerr << "ERROR: While processing link to <"
				  << page_state_->link_target << ">:\n";
		}

		// Print exception message.
		try
		{
		    throw;
		}
		catch (std::exception & e)
		{
		    std::cerr << "ERROR: " << e.what() << "\n";
		}
		catch (Glib::Exception & e)
		{
		    std::cerr << "ERROR: " << e.what() << "\n";
		}
		catch (...)
		{
		    std::cerr << "ERROR: Unknown exception\n";
		}

		Gtk::Main::quit();
	    }
	}

	// Call again if we're not done.
	return state_ != state_finished;
    }

    bool conversion_window::process()
    {
	assert(!page_queue_.empty());

	nsCOMPtr<nsIWebBrowser> browser(browser_widget_.get_browser());
	nsCOMPtr<nsIDocShell> doc_shell(do_GetInterface(browser));
	assert(doc_shell);
	nsCOMPtr<nsIDOMWindow> dom_window;
	check(browser->GetContentDOMWindow(getter_AddRefs(dom_window)));

	nsCOMPtr<nsIDOMDocument> basic_doc;
	check(dom_window->GetDocument(getter_AddRefs(basic_doc)));

	// Start or continue processing links.
	if (!page_state_.get())
	    page_state_.reset(
		new page_state(
		    get_screenshot(),
		    basic_doc, frame_params_.width, frame_params_.height));
	if (!process_links(page_state_.get(), basic_doc, doc_shell, dom_window))
	{
	    // We've finished work on the links so generate the
	    // menu VOB.
	    quantise_rgba_pixbuf(page_state_->diff_pixbuf,
				 dvd::button_n_colours);
	    generator_.generate_menu_vob(
		resource_map_[page_queue_.front()].index,
		page_state_->norm_pixbuf, page_state_->diff_pixbuf);

	    // Move on to the next page, if any, or else generate
	    // the DVD filesystem.
	    page_state_.reset();
	    page_queue_.pop();
	    if (!page_queue_.empty())
	    {
		load_next_page();
	    }
	    else
	    {
		generator_.generate(output_dir_);
		return false;
	    }
	}

	return true;
    }

    Glib::RefPtr<Gdk::Pixbuf> conversion_window::get_screenshot()
    {
	Glib::RefPtr<Gdk::Window> window(get_window());
	assert(window);
	window->process_updates(true);

	return Gdk::Pixbuf::create(Glib::RefPtr<Gdk::Drawable>(window),
				   window->get_colormap(),
				   0, 0, 0, 0,
				   frame_params_.width, frame_params_.height);
    }

    bool conversion_window::process_links(
	page_state * state,
	nsIDOMDocument * basic_doc,
	nsIDocShell * doc_shell,
	nsIDOMWindow * dom_window)
    {
	Glib::RefPtr<Gdk::Window> window(get_window());
	assert(window);

	nsCOMPtr<nsIDOMNSDocument> ns_doc(do_QueryInterface(basic_doc));
	assert(ns_doc);
#if MOZ_VERSION_GE(2,0,-1)
	nsCOMPtr<nsIDocument> doc(do_QueryInterface(basic_doc));
	assert(doc);
#endif
	nsCOMPtr<nsIPresShell> pres_shell;
	check(doc_shell->GetPresShell(getter_AddRefs(pres_shell)));
	nsCOMPtr<nsIEventStateManager> event_state_man(
	    get_event_state_manager(doc_shell));
	assert(event_state_man);
	nsCOMPtr<nsIDOMDocumentEvent> event_factory(
	    do_QueryInterface(basic_doc));
	assert(event_factory);
	nsCOMPtr<nsIDOMDocumentView> doc_view(do_QueryInterface(basic_doc));
	assert(doc_view);
	nsCOMPtr<nsIDOMAbstractView> view;
	check(doc_view->GetDefaultView(getter_AddRefs(view)));

	rectangle window_rect = {
	    0, 0, frame_params_.width, frame_params_.height
	};

	unsigned menu_index = resource_map_[page_queue_.front()].index;

	for (/* no initialisation */;
	     state->links_it != state->links_end;
	     ++state->links_it)
	{
	    nsCOMPtr<nsIDOMNode> node(*state->links_it);

	    // Find the link URI and separate any fragment from it.
	    nsCOMPtr<nsIURI> uri_iface;
#if MOZ_VERSION_GE(2,0,-1)
	    nsCOMPtr<nsIContent> content(do_QueryInterface(node));
	    assert(content);
	    uri_iface = content->GetHrefURI();
	    assert(uri_iface);
#else
	    nsCOMPtr<nsILink> link(do_QueryInterface(node));
	    assert(link);
	    check(link->GetHrefURI(getter_AddRefs(uri_iface)));
#endif
	    std::string uri, fragment;
	    {
		nsCString link_target_ns;
		check(uri_iface->GetSpec(link_target_ns));
		const char * str;
		PRUint32 len = NS_CStringGetData(link_target_ns, &str);
		state->link_target.assign(str, len);

		std::size_t hash_pos = state->link_target.find('#');
		uri.assign(state->link_target, 0, hash_pos);
		if (hash_pos != std::string::npos)
		    fragment.assign(state->link_target,
				    hash_pos + 1, std::string::npos);
	    }

	    // Is this a new link?
	    if (!state->link_changing)
	    {
		// Find a rectangle enclosing the link and clip it to the
		// window.
		nsCOMPtr<nsIDOMElement> elem(do_QueryInterface(node));
		assert(elem);
#if MOZ_VERSION_GE(2,0,-1)
		state->link_rect = get_elem_rect(doc, elem);
#else
		state->link_rect = get_elem_rect(ns_doc, elem);
#endif
		state->link_rect &= window_rect;

		if (state->link_rect.empty())
		{
		    std::cerr << "WARN: Ignoring invisible link to <"
			      << state->link_target << ">\n";
		    continue;
		}

		// Check whether this is a link to a video or a page then
		// add it to the known resources if not already seen; then
		// add it to the menu entries.
		dvd_generator::pgc_ref target;
		video_format format = video_format_from_uri(uri);
		if (format != video_format_none)
		{
		    PRBool is_file;
		    check(uri_iface->SchemeIs("file", &is_file));
		    if (!is_file)
			throw std::runtime_error(
			    "Link to video does not use file: scheme");
		    target = add_title(uri, format);
		    target.sub_index =
			std::strtoul(fragment.c_str(), NULL, 10);
		}
		else // video_format == video_format_none
		{
		    target = add_menu(uri);
		    // TODO: If there's a fragment, work out which button
		    // is closest and set target.sub_index.
		}

		generator_.add_menu_entry(menu_index,
					  state->link_rect, target);

		nsCOMPtr<nsIContent> content(do_QueryInterface(node));
		assert(content);
		nsCOMPtr<nsIDOMEventTarget> event_target(
		    do_QueryInterface(node));
		assert(event_target);

		nsCOMPtr<nsIDOMEvent> event;
		check(event_factory->CreateEvent(
			  NS_ConvertASCIItoUTF16("MouseEvents"),
			  getter_AddRefs(event)));
		nsCOMPtr<nsIDOMMouseEvent> mouse_event(
		    do_QueryInterface(event));
		assert(mouse_event);
		check(mouse_event->InitMouseEvent(
			  NS_ConvertASCIItoUTF16("mouseover"),
			  true,  // can bubble
			  true,  // cancelable
			  view,
			  0,     // detail: mouse click count
			  state->link_rect.left, // screenX
			  state->link_rect.top,  // screenY
			  state->link_rect.left, // clientX
			  state->link_rect.top,  // clientY
			  false, false, false, false, // qualifiers
			  0,     // button: left (or primary)
			  0));   // related target
		PRBool dummy;
		check(event_target->DispatchEvent(mouse_event,
						  &dummy));
		check(event_state_man->SetContentState(content,
						       NS_EVENT_STATE_HOVER));

		pres_shell->FlushPendingNotifications(Flush_Display);

		// We may have to exit and wait for image loading
		// to complete, at which point we will be called
		// again.
		if (browser_is_busy())
		{
		    state->link_changing = true;
		    return true;
		}
	    }

	    window->process_updates(true);

	    Glib::RefPtr<Gdk::Pixbuf> changed_pixbuf(
		Gdk::Pixbuf::create(
		    Glib::RefPtr<Gdk::Drawable>(window),
		    window->get_colormap(),
		    state->link_rect.left,
		    state->link_rect.top,
		    0,
		    0,
		    state->link_rect.right - state->link_rect.left,
		    state->link_rect.bottom - state->link_rect.top));
	    diff_rgb_pixbufs(
		state->norm_pixbuf,
		changed_pixbuf,
		state->diff_pixbuf,
		state->link_rect.left,
		state->link_rect.top,
		state->link_rect.right - state->link_rect.left,
		state->link_rect.bottom - state->link_rect.top);
	}

	return false;
    }

    const video::frame_params & lookup_frame_params(const char * str)
    {
	assert(str);
	static const char * const known_strings[] = {
	    "525",    "625",
	    "525/60", "625/50",
	    "NTSC",   "PAL",
	    "ntsc",   "pal"
	};
	for (std::size_t i = 0;
	     i != sizeof(known_strings)/sizeof(known_strings[0]);
	     ++i)
	    if (std::strcmp(str, known_strings[i]) == 0)
		return (i & 1)
		    ? video::frame_params_625
		    : video::frame_params_525;
	throw std::runtime_error(
	    std::string("Invalid video standard: ").append(str));
    }

    void print_usage(std::ostream & stream, const char * command_name)
    {
	stream <<
	    "Usage: " << command_name << " [gtk-options] [--preview]\n"
	    "           [--video-std {525|525/60|NTSC|ntsc"
	    " | 625|625/50|PAL|pal}]\n"
	    "           [--encoder {ffmpeg|mjpegtools}]\n"
	    "           menu-url [output-dir]\n";
    }
    
    void set_browser_preferences()
    {
	nsCOMPtr<nsIPrefService> pref_service;
	static const nsCID pref_service_cid = NS_PREFSERVICE_CID;
	check(CallGetService<nsIPrefService>(pref_service_cid,
					     getter_AddRefs(pref_service)));
	nsCOMPtr<nsIPrefBranch> pref_branch;
	check(pref_service->GetBranch("", getter_AddRefs(pref_branch)));

	// Disable IE-compatibility kluge that causes backgrounds to
	// sometimes/usually be missing from snapshots.  This is only
	// effective from Mozilla 1.8 onward.
	check(pref_branch->SetBoolPref(
		  "layout.fire_onload_after_image_background_loads",
		  true));

	// Set display resolution.  With standard-definition video we
	// will be fitting ~600 pixels across a screen typically
	// ranging from 10 to 25 inches wide, for a resolution of
	// 24-60 dpi.  I therefore declare the average horizontal
	// resolution to be 40 dpi.  The vertical resolution will be
	// slightly different but unfortunately Mozilla doesn't
	// support non-square pixels (and neither do fontconfig or Xft
	// anyway).

	// The browser.display.screen_resolution preference sets the
	// the nominal resolution for dimensions expressed in pixels.
	// (They may be scaled!)  In Mozilla 1.7 it also sets the
	// assumed resolution of the display - hence pixel sizes are
	// respected on-screen - but this is no longer the case in
	// 1.8.  Therefore it was renamed to layout.css.dpi in 1.8.1.
	// In 1.8 we need to set the assumed screen resolution
	// separately, but don't know how yet.  Setting one to 40
	// but not the other is *bad*, so currently we set neither.

#   	if 0
	    check(pref_branch->SetIntPref("browser.display.screen_resolution",
					  40));
#	endif
    }

} // namespace

void fatal_error(const std::string & message)
{
    std::cerr << "ERROR: " << message << "\n";
    Gtk::Main::quit();
}

int main(int argc, char ** argv)
{
    try
    {
	video::frame_params frame_params = video::frame_params_625;
	bool preview_mode = false;
	std::string menu_url;
	std::string output_dir;
	dvd_generator::mpeg_encoder encoder =
	    dvd_generator::mpeg_encoder_ffmpeg;

	// Do initial option parsing.  We have to do this before
	// letting Gtk parse the arguments since we may need to spawn
	// Xvfb first.
	int argi = 1;
	while (argi != argc)
	{
	    if (std::strcmp(argv[argi], "--") == 0)
	    {
		break;
	    }
	    else if (std::strcmp(argv[argi], "--help") == 0)
	    {
		print_usage(std::cout, argv[0]);
		return EXIT_SUCCESS;
	    }
	    else if (std::strcmp(argv[argi], "--preview") == 0)
	    {
		preview_mode = true;
		argi += 1;
	    }
	    else if (std::strcmp(argv[argi], "--video-std") == 0)
	    {
		if (argi + 1 == argc)
		{
		    std::cerr << "Missing argument to --video-std\n";
		    print_usage(std::cerr, argv[0]);
		    return EXIT_FAILURE;
		}
		frame_params = lookup_frame_params(argv[argi + 1]);
		argi += 2;
	    }
	    else
	    {
		argi += 1;
	    }
	}

	std::auto_ptr<x_frame_buffer> fb;
	if (!preview_mode)
	{
	    // Spawn Xvfb and set env variables so that Xlib will use it
	    // Use 8 bits each for RGB components, which should translate into
	    // "enough" bits for YUV components.
	    fb.reset(new x_frame_buffer(frame_params.width,
					frame_params.height,
					3 * 8));
	    setenv("XAUTHORITY", fb->get_authority().c_str(), true);
	    setenv("DISPLAY", fb->get_display().c_str(), true);
	}

	// Initialise Gtk
	Gtk::Main kit(argc, argv);

	// Complete option parsing with Gtk's options out of the way.
	argi = 1;
	while (argi != argc)
	{
	    if (std::strcmp(argv[argi], "--") == 0)
	    {
		argi += 1;
		break;
	    }
	    else if (std::strcmp(argv[argi], "--preview") == 0)
	    {
		argi += 1;
	    }
	    else if (std::strcmp(argv[argi], "--video-std") == 0)
	    {
		argi += 2;
	    }
	    else if (std::strcmp(argv[argi], "--save-temps") == 0)
	    {
		temp_file::keep_all(true);
		argi += 1;
	    }
	    else if (std::strcmp(argv[argi], "--encoder") == 0)
	    {
		if (argi + 1 == argc)
		{
		    std::cerr << "Missing argument to --encoder\n";
		    print_usage(std::cerr, argv[0]);
		    return EXIT_FAILURE;
		}
		if (std::strcmp(argv[argi + 1], "ffmpeg") == 0)
		{
		    encoder = dvd_generator::mpeg_encoder_ffmpeg;
		}
		else if (std::strcmp(argv[argi + 1], "mjpegtools") == 0)
		{
		    encoder = dvd_generator::mpeg_encoder_mjpegtools;
		}
		else
		{
		    std::cerr << "Invalid argument to --encoder\n";
		    print_usage(std::cerr, argv[0]);
		    return EXIT_FAILURE;
		}
		argi += 2;
	    }
	    else if (argv[argi][0] == '-')
	    {
		std::cerr << "Invalid option: " << argv[argi] << "\n";
		print_usage(std::cerr, argv[0]);
		return EXIT_FAILURE;
	    }
	    else
	    {
		break;
	    }
	}

	// Look for a starting URL or filename and (except in preview
	// mode) an output directory after the options.
        if (argc - argi != (preview_mode ? 1 : 2))
	{
	    print_usage(std::cerr, argv[0]);
	    return EXIT_FAILURE;
	}
	if (std::strstr(argv[argi], "://"))
	{
	    // It appears to be an absolute URL, so use it as-is.
	    menu_url = argv[argi];
	}
	else
	{
	    // Assume it's a filename.  Resolve it to an absolute URL.
	    std::string path(argv[argi]);
	    if (!Glib::path_is_absolute(path))
		path = Glib::build_filename(Glib::get_current_dir(), path);
	    menu_url = Glib::filename_to_uri(path);		
	}
	if (!preview_mode)
	    output_dir = argv[argi + 1];

	// Initialise Mozilla
	browser_widget::initialiser browser_init;
	set_browser_preferences();
	init_agent_style_sheet("file://" VIDEOLINK_SHARE_DIR "/videolink.css");
	init_agent_style_sheet(std::string("file://" VIDEOLINK_SHARE_DIR "/")
			       .append(frame_params.common_name).append(".css")
			       .c_str());
	if (!preview_mode)
	    null_prompt_service::install();

	// Run the browser/converter
	if (preview_mode)
	{
	    preview_window window(frame_params, menu_url);
	    window.show();
	    window.signal_hide().connect(sigc::ptr_fun(Gtk::Main::quit));
	    Gtk::Main::run();
	    return EXIT_SUCCESS;
	}
	else
	{
	    conversion_window window(frame_params, menu_url, output_dir, encoder);
	    window.show();
	    window.signal_hide().connect(sigc::ptr_fun(Gtk::Main::quit));
	    Gtk::Main::run();
	    return window.is_finished() ? EXIT_SUCCESS	: EXIT_FAILURE;
	}
    }
    catch (std::exception & e)
    {
	std::cerr << "ERROR: " << e.what() << "\n";
	return EXIT_FAILURE;
    }
}
