#include "video.hpp"

namespace video
{
    const struct frame_params frame_params_625 =
    {
	"pal",
	720, 576,
	25, 1,
	59, 54
    };

    const struct frame_params frame_params_525 =
    {
	"ntsc",
	720, 480,
	30000, 1001,
	10, 11
    };
}
