// Copyright 2008 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_EVENT_STATE_MANAGER_HPP
#define INC_EVENT_STATE_MANAGER_HPP

class nsIDocShell;
class nsIEventStateManager;

// This function works with nsPresContext, defined in
// <nsPresContext.h>, which includes <nsString.h>, which is mutually
// exclusive with including <nsStringAPI.h>.  What a mess.
nsIEventStateManager * get_event_state_manager(nsIDocShell * doc_shell);

#endif // !INC_EVENT_STATE_MANAGER_HPP
