// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_CHILD_ITERATOR_HPP
#define INC_CHILD_ITERATOR_HPP

#include <iterator>

#include "wchar_t_short.h"
#include <nsCOMPtr.h>
#include <nsIDOMNode.h>
#include "wchar_t_default.h"

class child_iterator
    : public std::iterator<std::input_iterator_tag, nsCOMPtr<nsIDOMNode>,
			   void, void, void>
{
public:
    child_iterator();
    explicit child_iterator(nsIDOMNode * node);
    ~child_iterator();

    already_AddRefed<nsIDOMNode> operator*() const;
    child_iterator & operator++();
    bool operator==(const child_iterator &) const;
    bool operator!=(const child_iterator & other) const
	{
	    return !(*this == other);
	}

private:
    nsIDOMNode * node_;
};

#endif // !INC_CHILD_ITERATOR_HPP
