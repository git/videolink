#ifndef INC_VIDEO_HPP
#define INC_VIDEO_HPP

namespace video
{
    struct frame_params
    {
	const char * common_name;
	unsigned int width, height;
	unsigned int rate_numer, rate_denom;
	unsigned int pixel_ratio_width, pixel_ratio_height;
    };

    extern const frame_params frame_params_625, frame_params_525;
}

#endif // !INC_VIDEO_HPP
