// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_XPCOM_SUPPORT_HPP
#define INC_XPCOM_SUPPORT_HPP

#include <stdexcept>

#include "wchar_t_short.h"
#include <nsError.h>
#include "wchar_t_default.h"

namespace xpcom_support
{
    void throw_exception(nsresult error);

    inline nsresult check(nsresult result)
    {
	if (NS_FAILED(result))
	    throw_exception(result);
	return result;
    }
}

#endif // !INC_XPCOM_SUPPORT_HPP
