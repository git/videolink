// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_LINK_ITERATOR_HPP
#define INC_LINK_ITERATOR_HPP

#include <iterator>

#include "wchar_t_short.h"
#include <nsCOMPtr.h>
#include <nsIDOMHTMLCollection.h>
#include <nsIDOMNode.h>
#include "wchar_t_default.h"

class nsIDOMDocument;

class link_iterator
    : public std::iterator<std::input_iterator_tag, nsCOMPtr<nsIDOMNode>,
			   void, void, void>
{
public:
    link_iterator();
    explicit link_iterator(nsIDOMDocument * document);

    already_AddRefed<nsIDOMNode> operator*() const;
    link_iterator & operator++();
    bool operator==(const link_iterator &) const;
    bool operator!=(const link_iterator & other) const
	{
	    return !(*this == other);
	}

private:
    nsCOMPtr<nsIDOMHTMLCollection> collection_;
    unsigned int index_, length_;
};

#endif // !INC_LINK_ITERATOR_HPP
