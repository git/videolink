// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_AUTO_PROC_HPP
#define INC_AUTO_PROC_HPP

#include <sys/types.h>

#include "auto_handle.hpp"

struct auto_proc_factory
{
    pid_t operator()() const { return -1; }
};

struct auto_kill_proc_closer
{
    void operator()(pid_t pid) const;
};
typedef auto_handle<pid_t, auto_kill_proc_closer, auto_proc_factory>
    auto_kill_proc;

struct auto_wait_proc_closer
{
    void operator()(pid_t pid) const;
};
typedef auto_handle<pid_t, auto_wait_proc_closer, auto_proc_factory>
    auto_wait_proc;

#endif // !INC_AUTO_PROC_HPP
