// Copyright 2008 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef MOZILLA_INTERNAL_API
#define MOZILLA_INTERNAL_API
#endif
#include "wchar_t_short.h"
#include <nsIDocShell.h>
#include <nsPresContext.h>
#include "wchar_t_default.h"

#include "event_state_manager.hpp"
#include "xpcom_support.hpp"

using xpcom_support::check;

nsIEventStateManager * get_event_state_manager(nsIDocShell * doc_shell)
{
    nsCOMPtr<nsPresContext> pres_context;
    check(doc_shell->GetPresContext(getter_AddRefs(pres_context)));
    return pres_context->EventStateManager();
}
