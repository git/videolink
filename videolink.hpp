#ifndef INC_VIDEOLINK_HPP
#define INC_VIDEOLINK_HPP

#include <string>

void fatal_error(const std::string & message);

#define MOZ_VERSION_EQ(major, minor, micro)				\
    (MOZ_VERSION_MAJOR == (major) &&					\
     MOZ_VERSION_MINOR == (minor) &&					\
     MOZ_VERSION_MICRO == (micro))
#define MOZ_VERSION_GE(major, minor, micro)				\
    (MOZ_VERSION_MAJOR > (major) ||					\
     (MOZ_VERSION_MAJOR == (major) &&					\
      (MOZ_VERSION_MINOR > (minor) ||					\
       (MOZ_VERSION_MINOR == (minor) && MOZ_VERSION_MICRO >= (micro)))))

#endif // !INC_VIDEOLINK_HPP
