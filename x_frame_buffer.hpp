// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_X_FRAME_BUFFER_HPP
#define INC_X_FRAME_BUFFER_HPP

#include <memory>
#include <string>

#include "auto_fd.hpp"
#include "auto_proc.hpp"
#include "temp_file.hpp"

// Run Xvfb with a frame buffer of the given dimensions.
class x_frame_buffer
{
public:
    x_frame_buffer(int width, int height, int depth);
    std::string get_authority() const;
    std::string get_display() const;

private:
    auto_fd tcp4_socket_, tcp6_socket_;
    int display_num_;
    std::auto_ptr<temp_file> auth_file_;
    auto_kill_proc server_proc_;
};

#endif // !INC_X_FRAME_BUFFER_HPP
