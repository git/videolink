// Copyright 2005-6 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_TEMP_FILE_HPP
#define INC_TEMP_FILE_HPP

#include <string>

class temp_file
{
public:
    explicit temp_file(const std::string & base_name);
    ~temp_file();

    void close();

    int get_fd() const { return fd_; }
    const std::string & get_name() const { return name_; }

    static void keep_all(bool keep);

private:
    int fd_;
    std::string name_;
};

class temp_dir
{
public:
    explicit temp_dir(const std::string & base_name);
    ~temp_dir();
    const std::string & get_name() const { return name_; }

private:
    std::string name_;
};

#endif // !INC_TEMP_FILE_HPP
