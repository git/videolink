// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#ifndef INC_AUTO_ARRAY_HPP
#define INC_AUTO_ARRAY_HPP

#include <cstddef>

// Like auto_ptr, but for arrays

template<typename element_type>
class auto_array_ref;

template<typename element_type>
class auto_array
{
    typedef auto_array_ref<element_type> ref_type;
public:
    auto_array()
	    : ptr_(0)
	{}
    explicit auto_array(element_type * ptr)
	    : ptr_(ptr)
	{}
    auto_array(ref_type other)
	    : ptr_(other.release())
	{}
    auto_array & operator=(auto_array & other)
	{
	    reset(other.release());
	}
    ~auto_array()
	{
	    reset();
	}
    element_type * get() const
	{
	    return ptr_;
	}
    element_type * release()
	{
	    element_type * ptr(ptr_);
	    ptr_ = 0;
	    return ptr;
	}
    void reset(element_type * ptr = 0)
	{
	    delete[] ptr_;
	    ptr_ = ptr;
	}
    element_type & operator[](std::ptrdiff_t index)
	{
	    return ptr_[index];
	}
    operator ref_type()
	{
	    return ref_type(*this);
	}
private:
    element_type * ptr_;
};

template<typename element_type>
class auto_array_ref
{
    typedef auto_array<element_type> target_type;
public:
    explicit auto_array_ref(target_type & target)
	    : target_(target)
	{}
    element_type * release()
	{
	    return target_.release();
	}
private:
    target_type & target_;
};

#endif // !INC_AUTO_ARRAY_HPP
