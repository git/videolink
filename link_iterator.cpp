// Copyright 2005 Ben Hutchings <ben@decadent.org.uk>.
// See the file "COPYING" for licence details.

#include "link_iterator.hpp"

#include <cassert>

#include <nsIDOMHTMLCollection.h>
#include <nsIDOMHTMLDocument.h>

link_iterator::link_iterator()
{}

link_iterator::link_iterator(nsIDOMDocument * document)
{
    nsCOMPtr<nsIDOMHTMLDocument> htmlDoc(do_QueryInterface(document));
    if (!htmlDoc)
	return;

    htmlDoc->GetLinks(getter_AddRefs(collection_));
    assert(collection_);

    index_ = 0;
    length_ = 0;
    collection_->GetLength(&length_);
    if (length_ == 0)
	collection_ = 0;
}

already_AddRefed<nsIDOMNode> link_iterator::operator*() const
{
    assert(collection_);
    nsIDOMNode * result = 0;
    collection_->Item(index_, &result);
    assert(result);
    return dont_AddRef(result);
}

link_iterator & link_iterator::operator++()
{
    assert(collection_);
    ++index_;
    if (index_ == length_)
	collection_ = 0;
    return *this;
}

bool link_iterator::operator==(const link_iterator & other) const
{
    return (collection_ == other.collection_
	    && (!collection_ || index_ == other.index_));
}
